#!/usr/bin/env bash

apt-get update
apt-get upgrade -y

wget https://downloads.plex.tv/plex-media-server-new/1.22.0.4163-d8c4875dd/debian/plexmediaserver_1.22.0.4163-d8c4875dd_amd64.deb

sudo dpkg -i plexmediaserver_1.22.0.4163-d8c4875dd_amd64.deb

sudo systemctl enable plexmediaserver.service
sudo systemctl start plexmediaserver.service
sudo systemctl status plexmediaserver.service

sudo ufw default deny incoming
sudo ufw enable
sudo ufw allow 80
sudo ufw allow 22
sudo ufw allow 32400
sudo ufw allow 3005
sudo ufw allow 5353
sudo ufw allow 8324
sudo ufw allow 1900
sudo ufw allow 32469
sudo ufw allow from any to any port 32410:32414 proto udp 