# Dokumentation LB02 - Plex-Server

## Inhaltsverzeichnis
- Einführung (Ziel des Projekts)
- Übersicht über Plex
- Vagrantfile
- install_plex.sh
-- Sicherheitsmassnahmen
- Probleme
- How to Use
- Quellenverzeichnis

### Einführung
> Das Ziel meines Projektes ist es, einen Plex-Server aufzusetzen. <br>
> Für die Umsetzung des Projekts muss Vagrant verwendet werden. <br>
> Es soll so viel wie möglich autmatisiert werden. Vor allem bei der Installation soll kein manueller User Input nötig sein. <br>
> Zusätzlich sollen auch noch Sicherheitsaspekte erkannt und umgesetzt werden. 

### Übersicht über Plex
> Plex ist ein Client-Server-Medienabspielsystem. Auf dem Server können Audio, Bilder oder Videoinhalte in Ordnern gesammelt werden, die dann im Player (App oder Webanwendung) aufgerufen und abgespielt werden können. Auch können diese Inhalte mit anderen Benutzern geteilt werden. 

### Vagrantfile
Anbei folgt nun erstmal eine Übersicht über das gesamte Vagrantfile, bevor die einzelnen Komponenten genauer angeschaut werden: <br>

    Vagrant.configure("2") do |config|

        config.vm.box = "generic/ubuntu2004"

        config.vm.provider "virtualbox" do |vb|
            vb.name = "M300-Plex-VM_3"
            vb.memory = "4096"
        end

        config.vm.network "forwarded_port", guest: 32400, host: 32400
        config.vm.network "forwarded_port", guest: 80, host: 8080

        config.vm.synced_folder "media/", "/home/vagrant/media", type:"virtualbox" 

        config.vm.provision :shell, path: "install_plex.sh"
    end

Nun widmen wir uns den einzelnen Abschnitten im Detail: <br>
> `Vagrant.configure("2") do |config|` <br>
> Hier wird die Konfigurationsversion definiert. Bei Vagrant gibt es hier **"1"** und **"2"**, wobei **"2"** die neuere und aktuelle ist. <br>
> Version **2** verfügt über viele neue Features und Konfigurationsmöglichkeiten im Vergleich zu Version **1**.

> `config.vm.box = "generic/ubuntu2004"` <br>
> Hier wird die **Vagrant Box** definiert. Diese kann man sich vorstellen wie ein Image oder ein Snapshot von einer Maschine. <br>
> In diesem Fall handelt es sich um eine Ubuntu 20.04 Maschine, welche bereits mit einem vordefinierten User daher kommt. <br>
> Der Benutzername ist **vagrant** und das Passwort ist ebenfalls **vagrant**. Hier sind auch schon Tools wie SSH oder ufw bereits installiert. <br>
> Link zur Box: https://app.vagrantup.com/generic/boxes/ubuntu2004  

>       config.vm.provider "virtualbox" do |vb|
>            vb.name = "M300-Plex-VM_3"
>            vb.memory = "4096"
>        end
> In diesem Teil wird der Provider definiert. In diesem Fall hier handelt es sich um Virtualbox. <br>
> Mit dieser Virtualisierungssoftware wird die VM dann gestartet. Nun kann man noch einige Konfigurationen machen. <br>
> Mit `vb.name` definiere ich den Namen, den die VM erhalten soll. Mit `vb.memory` wird definiert, wie viel RAM der VM zur Verfügung gestellt wird.

>        config.vm.network "forwarded_port", guest: 32400, host: 32400
>        config.vm.network "forwarded_port", guest: 80, host: 8080
> In diesem Abschnitt wird die Netzwerkkonfiguration der VM vorgenommen. <br>
> Bezüglich der IP-Adressierung habe ich hier nichts eigenes definiert. Die VM befindet sich nun in einem **NAT-Netzwerk** des Hosts. <br>
> Die Konfigurationen die ich hier aber vorgenommen habe, beziehen sich aber auf die Ports, die von Plex verwendet werden. <br>
> Es werden zwei sogenannte *Forwarded Ports* definiert. Das funktioniert so: Wenn auf dem Host der Port 8080 eingegeben wird, <br>
> wird es bei der VM auf Port 80 umgeleitet. Für sie ist es dann praktisch so, als käme die Anfrage von diesem Port.

>        config.vm.synced_folder "media/", "/home/vagrant/media", type:"virtualbox"
> Hier wird ein Shared Folder zwischen dem Host und der VM konfiguriert. Wobei das **/media** der relative Pfad auf dem Host ist und das **/home/vagrant/media** ist der Pfad auf der Linux VM. <br>
> In diesem Media Ordner befinden sich ein paar Bilder um die Plex-Installation testen zu können. In einer produktiven Umgebung könnte man so 
> bereits ausgewählte Medien auf den Guest bringen.

>        config.vm.provision :shell, path: "install_plex.sh"
> Mit dieser Zeile wird nun zum Schluss noch auf ein Bash-File verwiesen (:shell), welches ausgeführt werden soll. <br>
> Der relative Pfad zu diesem wird mit **path:** festgelegt.

### install_plex.sh
Hier eine Übersicht über das ganze **install_plex.sh** File:

    apt-get update
    apt-get upgrade -y

    wget https://downloads.plex.tv/plex-media-server-new/1.22.0.4163-d8c4875dd/debian/plexmediaserver_1.22.0.4163-d8c4875dd_amd64.deb

    sudo dpkg -i plexmediaserver_1.22.0.4163-d8c4875dd_amd64.deb

    sudo systemctl enable plexmediaserver.service
    sudo systemctl start plexmediaserver.service
    sudo systemctl status plexmediaserver.service

    sudo ufw default deny incoming
    sudo ufw enable
    sudo ufw allow 80
    sudo ufw allow 22
    sudo ufw allow 32400
    sudo ufw allow 3005
    sudo ufw allow 5353
    sudo ufw allow 8324
    sudo ufw allow 1900
    sudo ufw allow 32469
    sudo ufw allow from any to any port 32410:32414 proto udp 

Nun die einzelnen Abschnitte im Detail:
>       apt-get update
>       apt-get upgrade -y
> In diesem Abschnitt werden die neusten Updates für die Software der VM installiert. <br>
> Mit **apt-get update** werden Updates für die Packages gesucht. Mit **apt-get upgrade -y** werden die Updates installiert. <br>
> Das *-y* steht dabei für **yes**, also die Bestätigung, die man beim manuellen Ausführen geben müsste.

>       wget https://downloads.plex.tv/plex-media-server-new/1.22.0.4163-d8c4875dd/debian/plexmediaserver_1.22.0.4163-d8c4875dd_amd64.deb
> Dieser Befehl lädt das Debian-Paket (.deb File) herunter, mit welchem wir später die Softwareinstallation von Plex durchführen. 

>       sudo dpkg -i plexmediaserver_1.22.0.4163-d8c4875dd_amd64.deb
> Dieser Befehl führt die Softwareinstallation mit dem eben heruntergeladenen FIle durch.

>       sudo systemctl enable plexmediaserver.service
>       sudo systemctl start plexmediaserver.service
>       sudo systemctl status plexmediaserver.service
> Nun da Plex installiert ist, muss der Service noch gestartet werden. Mit der **ersten Zeile** wird der Service aktiviert, mit der **zweiten Zeile** wird der Service **gestartet** und die **dritte Zeile** gibt den **Status** des Dienstes aus. 

#### Sicherheitsmerkmale
Um einen Sicherheitsaspekt in das ganze Projekt reinzubringen, wurde die ufw konfiguriert. Die Konfiguration findet im **install_plex.sh** File statt. 

    sudo ufw default deny incoming
    sudo ufw enable

Zuerst wird festgelegt, dass generell aller reinkommender Traffic blockiert wird. Anschliessend wird die Firewall aktiviert. Zu diesem Zeitpunkt funktioniert der Zugriff auf Plex nicht mehr.

    sudo ufw allow 80
    sudo ufw allow 22
    sudo ufw allow 32400
    sudo ufw allow 3005
    sudo ufw allow 5353
    sudo ufw allow 8324
    sudo ufw allow 1900
    sudo ufw allow 32469
    sudo ufw allow from any to any port 32410:32414 proto udp 

Nun werden alle Ports freigegeben, welche für Plex und den Serverzugriff benötigt werden. Dadurch dass der Zugriff nur auf diese Ports beschränkt ist, besteht eine höhere Sicherheit als wenn dies nicht der Fall wäre.

### Probleme
Im Laufe der Umsetzung trat folgender Fehler mehrfach auf, nachdem `vagrant up` ausgeführt wurde. 

    The name of your virtual machine couldn't be set because VirtualBox
    is reporting another VM with that name already exists. Most of the
    time, this is because of an error with VirtualBox not cleaning up
    properly. To fix this, verify that no VMs with that name do exist
    (by opening the VirtualBox GUI). If they don't, then look at the
    folder in the error message from VirtualBox below and remove it
    if there isn't any information you need in there.

    VirtualBox error:

    VBoxManage.exe: error: Could not rename the directory 'C:\Users\eoliver\VirtualBox VMs\generic-ubuntu2004-virtualbox_1616159531521_95906' to        'C:\Users\eoliver\VirtualBox VMs\M300-Plex-VM_2' to save the settings file (VERR_ALREADY_EXISTS)
    VBoxManage.exe: error: Details: code E_FAIL (0x80004005), component SessionMachine, interface IMachine, callee IUnknown
    VBoxManage.exe: error: Context: "SaveSettings()" at line 3249 of file VBoxManageModifyVM.cpp``

Woher der Fehler genau kommt, konnte ich nicht herausfinden. Allerdings habe ich den Workaround verwendet, dass ich im Vagrantfile in folgender Zeile den Namen der VM abgeändert habe:
    vb.name = "M300-Plex-VM_3"
Das ist auch der Grund, weshalb der Namen den *"_3"* hat. Wenn der Name geändet wird, geht es danach wieder.

### How to Use
Um die VM jetzt zu erstellen, muss man in einer Shell (**Git-Bash** oder CMD/Powershell) mit Hilfe des `cd` Befehls in den gleichen Ordner gehen in dem das Vagrantfile liegt. <br>
Nun kann man mit folgenden Befehl die VM erstellen und die Konfiguration anwenden:

    vagrant up

Nun folgt in der Shell ein Haufen Output. Wenn kein neuer mehr kommt und die letzte Meldung kein Error ist, kann man davon ausgehen, dass es funtktioniert hat. <br>
Das Ganze muss man nun natürlich auch testen. Dies tut man, indem man im Webbrowser folgende URL eingibt: `127.0.0.1:32400/web` <br>
Anschliessend sollte das Webinterface von Plex erscheinen. <br>

![alt text](../2021-03-24_08-42-38.png)

Nun muss man in Plex nur noch die normalen Anmeldungs-/Konfigschritte durchlaufen. Anschliessend kann man auch den SharedFolder als Library hinzufügen, um zu testen, ob die Einbindung funktioniert hat. (siehe Bild oben)

### Quellenverzeichnis
- https://nspeaks.com/how-to-install-and-use-plex-media-server-on-ubuntu-18-04/
- https://linuxhint.com/install_plex_ubuntu-2/
- https://github.com/edorgeville/vagrant-plex (angeschaut aber keinen Code kopiert)
- https://de.wikipedia.org/wiki/Plex 
