# Dokumentation LB3

## Inhaltsverzeichnis
- [Einleitung](https://gitlab.com/eschbach125/m300_lb/-/blob/master/lb3/README.md#einleitung)
- [Übersicht über den Service](https://gitlab.com/eschbach125/m300_lb/-/blob/master/lb3/README.md#%C3%BCbersicht-%C3%BCber-den-service)
- [Übersicht über die Umgebung](https://gitlab.com/eschbach125/m300_lb/-/blob/master/lb3/README.md#%C3%BCbersicht-%C3%BCber-die-umgebung)
- [Beschreibung Eigenanteil](https://gitlab.com/eschbach125/m300_lb/-/blob/master/lb3/README.md#beschreibung-eigenanteil)
- [Service Anwendung](https://gitlab.com/eschbach125/m300_lb/-/blob/master/lb3/README.md#service-anwendung)
    - [Production Modus](https://gitlab.com/eschbach125/m300_lb/-/blob/master/lb3/README.md#production-modus)
    - [Developement Modus](https://gitlab.com/eschbach125/m300_lb/-/blob/master/lb3/README.md#developement-modus)
    - [Zugrff auf Snipe-It](https://gitlab.com/eschbach125/m300_lb/-/blob/master/lb3/README.md#zugriff-auf-snipe-it)
    - [Service beenden](https://gitlab.com/eschbach125/m300_lb/-/blob/master/lb3/README.md#service-beenden)
- [Probleme mit dem nginx-Proxy](https://gitlab.com/eschbach125/m300_lb/-/blob/master/lb3/README.md#probleme-mit-dem-nginx-proxy)
- [Implementierte Sicherheitsmerkmale](https://gitlab.com/eschbach125/m300_lb/-/blob/master/lb3/README.md#implementierte-sicherheitsmerkmale)
- [Codedokumentation](https://gitlab.com/eschbach125/m300_lb/-/blob/master/lb3/README.md#codedokumentation)
    - [docker-compose.yml](https://gitlab.com/eschbach125/m300_lb/-/blob/master/lb3/README.md#docker-composeyml)
        - [snipe-mysql](https://gitlab.com/eschbach125/m300_lb/-/blob/master/lb3/README.md#snipe-mysql)
        - [snipe-it](https://gitlab.com/eschbach125/m300_lb/-/blob/master/lb3/README.md#snipe-it)
        - [nginx-proxy](https://gitlab.com/eschbach125/m300_lb/-/blob/master/lb3/README.md#nginx-proxy)
        - [nginx-proxy-gen und nginx-proxy-le](https://gitlab.com/eschbach125/m300_lb/-/blob/master/lb3/README.md#nginx-proxy-gen-und-nginx-proxy-le)
        - [Volumes](https://gitlab.com/eschbach125/m300_lb/-/blob/master/lb3/README.md#volumes)
    - [Dockerfile](https://gitlab.com/eschbach125/m300_lb/-/blob/master/lb3/README.md#dockerfile)
    - [nginx.conf](https://gitlab.com/eschbach125/m300_lb/-/blob/master/lb3/README.md#nginxconf)
    - [nginx.tmpl und www.conf](https://gitlab.com/eschbach125/m300_lb/-/blob/master/lb3/README.md#nginxtmpl-und-wwwconf)
- [Testfälle](https://gitlab.com/eschbach125/m300_lb/-/blob/master/lb3/README.md#testf%C3%A4lle)
    - [Testfall 1 - Produktionsmodus und Zugriff über Webbrowser](https://gitlab.com/eschbach125/m300_lb/-/blob/master/lb3/README.md#testfall-1-produktionsmodus-und-zugriff-%C3%BCber-webbrowser)
    - [Testfall 2 - Developementmodus und Zugriff über Webbrowser](https://gitlab.com/eschbach125/m300_lb/-/blob/master/lb3/README.md#testfall-2-developementmodus-und-zugrff-%C3%BCber-webbrowser)
    - [Testfall 3 - Zugriff über Reverse-Proxy](https://gitlab.com/eschbach125/m300_lb/-/blob/master/lb3/README.md#testfall-3-produktionsmodus-und-zugriff-%C3%BCber-reverse-proxy)
- [Quellenverzeichnis](https://gitlab.com/eschbach125/m300_lb/-/blob/master/lb3/README.md#quellenverzeichnis)
    - [Quellen bei Problemen](https://gitlab.com/eschbach125/m300_lb/-/blob/master/lb3/README.md#quellen-bei-problemen)

## Einleitung
Das ist die Dokumentation zu meiner LB03 im Modul M300. <br>
Der Auftrag war, einen Service mit Docker-Compose aufzusetzen und entsprechend konfigurieren. <br>
Weitere Rahmenbedingungen waren, dass man mehr als einen Container verwendet, ein eigenes Image verwendet, merged Docker-Compose Files verwendet und Sicherheitsmerkmale einbaut. <br>

## Übersicht über den Service
Als Service habe ich Snipe-It gewählt. Snipe-It ist eine webbasierte Inventarsoftware. Hier die offizielle Beschreibung: <br>
*Snipe-IT was made for IT asset management, to enable IT departments to track who has which laptop, when it was purchased, which software licenses and accessories are available, and so on.* <br>

Snipe-It unterstützt zum Beispiel auf die Integrierung von AD-Usern, was für Unternehmen sehr praktisch sein kann. <br>
Damit der Service funktionieren kann, braucht er eine Datenbank, was ihn für dieses Projekt sehr geeignet macht, da so schon mal zwei Container gebraucht werden können.

## Übersicht über die Umgebung


![Grafik](../Leeres_Diagramm_-_Seite_1.png)

Die Umgebung besteht aus 5 Containern. Der snipe-it Container beinhaltet die eigentliche Webapplikation und ist abhängig vom snipe-mysql Container, welcher die Datenbank beinhaltet. <br>
Der nginx-proxy ist das selbst erstellte Image, welches ein Alpine Linux ist auf welchem ich dann den Nginx installiert habe. <br>
Zusätzlich zu diesem hat es für den Proxy noch zwei weitere Container. Der eine wäre nginx-proxy-gen, welcher dafür da wäre Files in den Containern zu generieren, in diesem Fall z.B. eine nginx.conf und der andere wäre der nginx-proxy-le, welcher für letsencyrpt zuständig ist.

Aufgebaut und getestet wurde die Umgebung mit der Docker Vagrant VM aus dem M300 Repository.

## Beschreibung Eigenanteil
In diesem Abschnitt wird kurz beschrieben, was alles an der Arbeit Eigenleistung war und wo auf bereits vorhandenes zurückgegriffen wurde. <br>
Die Container snipe-it, snipe-mysql und nginx-proxy (ebenfalls das Image mit dem Dockerfile) wurden komplett alleine erstellt. <br>
Ebenfalls wurde das nginx.conf File selbststänfig erstellt und angepasst. <br>
Die Container nginx-proxy-gen und nginx-proxy-le sowie das nginx.tmpl wurden aus einer frühreren Arbeit übernommen und angepasst. <br>

## Service Anwendung
Um den Service zu verwenden, gibt es zwei verschiedene Möglichkeiten. Einmal kann man ihn im Production Modus starten oder im Developement Modus. <br>
Die ersten Schritte sind aber generell gleich. Der Service muss auf einer Linux Maschine ausgeführt werden, getestet wurde das ganze auf Ubuntu. <br>
Zuerst sollte man dieses Repository klonen (alternativ kann man die Files auch manuell reinkopieren): <br>

    git clone https://gitlab.com/eschbach125/m300_lb.git 
Anschliessend muss man in das Verzeichnis `lb3` navigieren: <br>

    cd m300_lb/lb3 
Ab hier teilt sich das ganze dann auf. <br>
### Production Modus
Um Snipe-It im Production Modus zu starten, muss folgender Command eingegeben werden: <br>

    docker-compose --compatibility up -d
Mit `--compatibility` wird das Limitieren der Systemressourcen ermöglicht, `-d` oder die Langform `--detach` ermöglicht, dass die Container im Hintergrund laufen und man die Shell weiterhin verwenden kann. <br>
### Developement Modus
Um Snipe-It im Developement Modus zu starten, muss folgender Command eingegeben werden: <br>

    docker-compose --compatibility -f docker-compose.yml -f docker-compose.dev.yml up -d
Hier wird mit `-f` noch auf ein zweites Docker-Compose File mit dem Zusatz **.dev** verwiesen. In diesem wird die Konfiguration des snipe-it Containers so angepasst, dass dieser im Developement Modus läuft und der Debug Modus aktiviert ist. <br>

### Zugriff auf Snipe-It
Der Zugriff auf Snipe-It erfolgt in beiden Fällen mit folgender Eingabe im Webbrowser des Hosts: <br>

    http://192.168.60.101:3051
Zu Beginn sollte der Setupscreen angezeigt werden.
### Service beenden
Mit folgendem Command kann Snipe-It wieder beendet werden: <br>

    docker-compose down

## Probleme mit dem nginx-Proxy
Leider muss ich mir eingestehen, dass ich es nicht geschafft habe, den nginx Reverse-Proxy zum Laufen zu bringen. <br>
Nach einem zusätzlichen Zeitaufwand von ca. 8-10 Stunden in meinen Ferien (auch in der Commit History ersichtlich), läuft der Proxy leider noch immer nicht. <br>
Mein eigentliches Ziel wäre gewesen, den Service im Webserver nur mit `http://192.168.60.101` aufzurufen, was dann vom Proxy weitergeleitet worde wäre. <br>
Wenn man dies nun aber aufruft, kommt man zwar auf eine Seite von nginx, was heisst das dieser läuft und das selbstgemachte Image funktioniert, allerdings wartet dort auch eine Fehlermeldung. <br>
Im Laufe des Projekts hatte ich dort bereits sämtliche Fehlercodes von **403** über **404** bis zu **502**. <br>
Für jeden gelösten Fehler tauchte ein neuer auf und meine Recherchen im Internet verliefen irgendwann im Sand. <br>
Um die LB03 nun trotzdem noch zeitgemäss und vollständig abzugeben, verzichte ich darauf, dieses Problem weiter zu verfolgen, <br>
in der Hoffnung, dass die Erstellung des Images jetzt nicht als unnötig angesehen wird. <br>
Die Funktionalität von Snipe-It wird durch dieses Problem nicht beeinflusst. <br>

## Implementierte Sicherheitsmerkmale
Als Sicherheitsmerkmal habe ich für die snipe-it und nginx-proxy Container die Systemressourcen limitiert (Genaue Infos in der Codedokumentation).
## Codedokumentation
### docker-compose.yml
Hier folgt nun die Dokumentation zum docker-compose.yml File. Diese werde ich auf die jeweiligen Container aufteilen. <br>
#### snipe-mysql
    snipe-mysql:
            container_name: snipe-mysql
            image: mysql:5.6
            environment: 
                - MYSQL_PORT_3306_TCP_ADDR=snipe-mysql
                - MYSQL_ROOT_PASSWORD=snipeit
                - MYSQL_DATABASE=snipeit
                - MYSQL_USER=snipeit
                - MYSQL_PASSWORD=snipeit1234
            volumes:
                - snipesql-vol:/var/lib/mysql
            command: --default-authentication-plugin=mysql_native_password
            expose:
                - '3306'
Als Image wird das offizielle MySQL Image verwendet, allerdings in der Version **5.6**. Dies ist so in der Snipe-It Dokumentation beschrieben, <br>
da Snipe-It mit den neueren MySQL-Versionen nicht kompatibel ist. <br>
Die Konfiguration unter `environment` ist relativ selbsterklärend. Hier wird ein Passwort für den root User festgelegt sowie Username und Passwort für den snipeit User. <br>
Ausserdem wird der Name der Datenbank festgelegt. <br>
Unter dem Punkt `volumes` wird ein **persistentes Volume** definiert. <br> 
Dieses wird gebraucht damit nach einem Neustart des Containers nicht alle Daten von Snipe-It verloren gehen. Die ganze Datenbank wird somit auch auf das Host-System gespeichert. <br>
Der Command `--default-authentication-plugin=mysql_native_password` definiert, dass mysql_native_password als Authentication Plugin verwendet wird. <br>
Mehr Informationen dazu findet man hier: https://mariadb.com/kb/en/authentication-plugin-mysql_native_password/ <br>
Zuletzt wird noch mit `expose` der Port 3306 (Standardport von mysql) für alle anderen Container im Netzwerk zugänglich gemacht. <br>

#### snipe-it
    snipe-it:
        container_name: snipe-it
        image: snipe/snipe-it
        depends_on: 
            - snipe-mysql
        environment:
            - APP_KEY=base64:t0N6orh/XHbyqTqpuvS2qpkVynMvbari84Mitm/CHsM=
            - APP_ENV=production
            - APP_DEBUG=false
            - APP_URL=http://127.0.0.1:3051
            - APP_LOCALE=en
            - APP_TRUSTED_PROXIES=192.168.60.101
            - DB_PORT=3306
            - DB_CONNECTION=mysql
            - DB_HOST=snipe-mysql
            - DB_DATABASE=snipeit
            - DB_USERNAME=snipeit
            - DB_PASSWORD=snipeit1234
            - VIRTUAL_PORT=80
            - VIRTUAL_HOST=192.168.60.101
        deploy:
            resources:
                limits:
                    cpus: '0.2'
                    memory: 256M
        ports: 
            - '3051:80'
Als Image wird hier das offizielle Docker Image vom Snipe-It verwendet. <br>
Unter `depends_on` wird definiert, dass dieser Container nur laufen kann, wenn der **snipe-mysql** Container auch läuft. <br>
Damit Snipe-It funktioniert, braucht es einen gültigen "APP_KEY". Ausserdem wird in diesem File festgelegt, dass die App im `production` Modus läuft und der `Debug` Modus aus ist. <br>
Es wird auch festgelegt, dass die eigentlich URL `127.0.0.1:3051` sein soll und dass dem Proxy 192.168.60.101 vertraut werden soll. <br>
Die "DB" Variablen sind mit den im snipe-mysql festgelegten Werten identisch, damit die Verbindung zur Datenbank nachher funktionieren kann. <br>
Unter `deploy` werden Limiten für Systemressourcen definiert. Snipe-It darf höchstens 20% der CPU-Kapazität beanspruchen und 256MB RAM verwenden. <br>
Zum Schluss wird noch der von aussen einkommende Traffic von Port 3051 auf Port 80 gemappt. <br>
#### nginx-proxy
    nginx-proxy:
        container_name: nginx-proxy
        build: .
        image: nginx_proxy
        depends_on:
            - snipe-it
        deploy:
            resources:
                limits:
                    cpus: '0.6'
                    memory: 512M
        ports:
            - "80:80"
            - "443:443"
        volumes:
            - conf:/etc/nginx/conf.d
            - vhost:/etc/nginx/vhost.d
            - html:/usr/share/nginx/html
            - certs:/etc/nginx/certs:ro
        labels:
            - "com.github.jrcs.letsencrypt_nginx_proxy_companion.nginx_proxy"
        restart: always
Bei diesem Container wird ein eigenes Image gebaut. Dies geschieht unter dem Punkt `build`. Da das Dockerfile im gleichen Verzeichnis liegt, wird nur mit "." darauf verwiesen. <br> 
Das "nginx_proxy" und `image` gibt dem Image dann auch diesen Namen. <br>
Auch dieser Container darf wiederum nur laufen, wenn der Container "snipe-it" läuft. <br>
Ebenfalls werden hier die Systemressourcen nach dem gleichen Prinzip wie beim anderen Container limitiert. <br>
Unter `volumes` werden vier persistente Volumes erstellt. Und zwar für einige wichtige Ordner, welche bei einem Absturz von nginx nicht verloren gehen dürfen. <br>
Das `label` ist notwendig, damit der letsencrypt Container weiss, welchen nginx-Container er verwenden soll. <br>
Unter `restart` wird noch festgelegt, dass der Container nach einem Absturz immer probieren soll wieder zu starten. <br>
#### nginx-proxy-gen und nginx-proxy-le
    docker-gen:
        image: jwilder/docker-gen
        command: -notify-sighup nginx-proxy -watch /etc/docker-gen/templates/nginx.tmpl /etc/nginx/conf.d/default.conf
        container_name: nginx-proxy-gen
        depends_on:
            - nginx-proxy
        volumes:
            - conf:/etc/nginx/conf.d
            - vhost:/etc/nginx/vhost.d
            - certs:/etc/nginx/certs:ro
            - ./nginx.tmpl:/etc/docker-gen/templates/nginx.tmpl:ro
            - /var/run/docker.sock:/tmp/docker.sock:ro
        labels:
            - "com.github.jrcs.letsencrypt_nginx_proxy_companion.docker_gen"
        restart: always
    letsencrypt:
        image: jrcs/letsencrypt-nginx-proxy-companion
        container_name: nginx-proxy-le
        depends_on:
            - nginx-proxy
            - docker-gen
        volumes:
            - vhost:/etc/nginx/vhost.d
            - html:/usr/share/nginx/html
            - certs:/etc/nginx/certs
            - /var/run/docker.sock:/var/run/docker.sock:ro
        restart: always
Diese Container habe ich, wie oben erwähnt, aus einem früheren Projekt übernommen. <br>
Ehrlich gesagt bin ich der Meinung, dass diese für die Funktionalität des Reverse-Proxys eigentlich gar nicht unbedingt notwendig wären, aber in meiner Verzweiflung habe ich diese trotzdem irgendwann reingenommen. <br>
Auf ein genaue Dokumentation der Docker-Compose Konfiguration dieser Container möchte ich verzichten, weil sie eigentlich über die genau gleichen Eigenschaften wie der nginx-proxy Container verfügen und sowieso nicht funktionieren. <br>
#### Volumes
    volumes:
        snipesql-vol:
        conf:
        vhost:
        html:
        certs:
Ganz zum Schluss vom Dokument sind noch alle Volumes definiert. <br>
### docker-compose.dev.yml
    version: '3'

    services:
        snipe-it:
            environment: 
                - APP_DEBUG=true
                - APP_ENV=developement
Dieses File ist zwar recht klein, aber erfüllt seinen Zweck. Als 'merged' Compose File verwendet, überschreibt es die beiden Eigenschaften `APP_DEBUG` und `APP_ENV` wodurch der Debug und Developement Modus aktiviert werden, ohne dass man im richtigen Docker-Compose File Änderungen vornehmen muss. <br>
### Dockerfile
    FROM alpine:latest
    RUN apk update -U upgrade
    RUN apk add nginx 
    RUN adduser -D -g 'www' www 
    RUN mkdir /www 
    RUN chown -R www:www /var/lib/nginx 
    RUN chown -R www:www /www 
    RUN rm /etc/nginx/nginx.conf
    COPY /nginx.conf /etc/nginx/nginx.conf
    RUN mkdir /var/run/nginx
    RUN touch /var/run/nginx/nginx.pid
    RUN apk add php7-fpm
    RUN apk add php
    RUN apk add php7-opcache
    RUN apk add openrc
    RUN openrc
    RUN touch /run/openrc/softlevel
    RUN rc-service php-fpm7 start
    RUN rm /etc/php7/php-fpm.d/www.conf
    COPY /www.conf /etc/php7/php-fpm.d/www.conf
    RUN rc-service php-fpm7 restart
    CMD nginx -g "daemon off;"
    EXPOSE 80
Im Dockerfile wird mein nginx-proxy Image erstellt. <br>
Die Grundlage dafür ist das aktuellste `alpine` Image. Dieses ist recht schlank und so gut für die Performance. <br>
Anschliessend werden Updates gesucht und installiert. <br>
Dann folgt die Installation von nginx. Für diesen wird dann noch ein User erstellt und Berechtigungen angepasst. <br>
Nun wird das `nginx.conf` File gelöscht und das selber erstellte File in den Container an dessen Stelle kopiert. <br>
Anschliessend muss noch `php7-fpm` installiert werden. Dies wird benötigt damit Snipe-It mit nginx funktioniert. Das habe ich der offiziellen Dokumentation von Snipe-It entnommen. <br>
Damit man diesen Service starten kann, werden anschliessend noch `openrc` Tools installiert. Diesen fehlt aber ein File namens "softlevel", welches ich anschliessend erstellt. <br>
Nun wird die Konfiguration des php7-fpm Services in den Container kopiert, bevor die beiden Services gestartet werden. <br>
Dabei musste ich feststellen, dass man nginx mit dem Command `nginx -g "daemon off;"` starten muss, damit der Container nicht mit Code 0 exited. <br>
Zum Schluss wird noch der Port 80 exposed <br>
### nginx.conf
    http {
        include                     /etc/nginx/mime.types;
        default_type                application/octet-stream;
        sendfile                    on;
        access_log                  /var/log/nginx/access.log;
        keepalive_timeout           3000;
        server {
            listen                  80;
            server_name             192.168.60.101;
            client_max_body_size    32m;
            error_page              500 502 503 504  /50x.html;

            location = / {
                proxy_pass          http://192.168.60.101:3051;
                proxy_set_header    X-Forwarded-Proto $scheme;
                alias               /var/www/html/public;
                index               index index.php index.html index.htm;
                try_files $uri $uri/ /index.php$is_args$args;
            }
            location ~ \.php$ {
                try_files $uri $uri/ =404;
                fastcgi_pass unix:/var/run/php5-fpm-www.sock;
                fastcgi_index index.php;
                fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                include fastcgi_params;
            }
            location = /50x.html {
                root              /var/lib/nginx/html;
            }
Der Vollständigkeit halber hier noch der Ausschnitt aus dem nginx.conf File, welchen ich bearbeitet habe. <br>
Das ist nun die finale Version, bei der ich es belassen habe. Gut möglich, dass ich in einem früheren Commit mal näher an einer korrekten Lösung war. <br>
Das ganze habe ich eigentlich zusammengestellt aus der offiziellen Snipe-It Dokumentation zu dem Thema und meiner Internet-Recherche. <br>
Der finale Stand war eigentlich, dass ich nginx nicht dazu bringen konnte, die Files aus dem snipe-it Container zu verwenden. <br> 
So gab er mir immer einen Error 404, weil er die Files im eigenen Container gesucht hat. <br>
### nginx.tmpl und www.conf
Auf diese Files werde ich nicht im Detail eingehen, aber kurz beschreiben, was sie machen. <br>
Das nginx.tmpl File wird für den nginx-proxy-gen Container gebraucht. Es gibt eigentlich dessen Konfiguration vor und was in gewissen Szenarios passieren soll.
Das www.conf File ist die von mir angepasste Konfiguration für den php7-fmp Service. Angepasst habe ich aber nur diese Zeile:
    listen = /var/run/php7-fpm.sock
## Testfälle
Folgend werden drei Testfälle beschrieben, mit denen die Applikation getestet wurde:
### Testfall 1 - Produktionsmodus und Zugriff über Webbrowser
Der Service wird genau wie in "Anwendung des Service" und dem Unterkapitel "Production Modus" beschrieben gestartet:

    git clone https://gitlab.com/eschbach125/m300_lb.git 
    cd m300_lb/lb3 
    docker-compose --compatibility up -d
Anschliessend wird auf dem Webbrowser auf den Service zugegriffen:

    http://192.168.60.101:3051
![Grafik](../prod.png)
Man sieht, dass der Test erfolgreich war. Man hat Zugriff auf den Service, es gibt keine Fehler und keine Warnungen. <br>
Dass keine Warnungen kommen heisst gleichzeitig auch, dass man sich im Produktions Modus befindet und der Debugger ausgeschaltet ist. <br>
### Testfall 2 - Developementmodus und Zugrff über Webbrowser
Der Service wird genau wie in "Anwendung des Service" und dem Unterkapitel "Developement Modus" beschrieben gestartet:

    git clone https://gitlab.com/eschbach125/m300_lb.git 
    cd m300_lb/lb3 
    docker-compose --compatibility -f docker-compose.yml -f docker-compose.dev.yml up -d
Anschliessend wird auf dem Webbrowser auf den Service zugegriffen:

    http://192.168.60.101:3051
![Grafik](../dev.png)
An der gelben Warnung sieht man, dass der Test erfolgreich war, da sich Snipe-It nun im Developement Modus befindet. <br>
Auch der rote Balken unten zeigt, dass es funktioniert hat, da dies der Debug Modus ist. <br>
### Testfall 3 - Produktionsmodus und Zugriff über Reverse-Proxy
Der Service wird genau wie in "Anwendung des Service" und dem Unterkapitel "Production Modus" beschrieben gestartet:

    git clone https://gitlab.com/eschbach125/m300_lb.git 
    cd m300_lb/lb3 
    docker-compose --compatibility up -d
Anschliessend wird auf dem Webbrowser auf den Service zugegriffen:

    http://192.168.60.101
![Grafik](../nginx.png)
Wie man sieht, ist dieser Test fehlgeschlagen. Man erhält den Fehler 404, sprich die gewünschten Files wurden nicht gefunden. <br>
## Quellenverzeichnis
- https://snipe-it.readme.io/docs/docker
- https://snipe-it.readme.io/docs/linuxosx
- https://stackoverflow.com/questions/50230399/what-is-the-difference-between-docker-compose-build-and-docker-build
- https://hub.docker.com/_/alpine
- https://ostechnix.com/how-to-upgrade-alpine-linux-to-latest-version/
- https://stackoverflow.com/questions/42345235/how-to-specify-memory-cpu-limit-in-docker-compose-version-3
  
### Quellen bei Problemen
- https://wiki.alpinelinux.org/wiki/Nginx
- https://github.com/nginx-proxy/nginx-proxy 
- https://github.com/gliderlabs/docker-alpine/issues/183
- https://github.com/gliderlabs/docker-alpine/issues/185
- https://serverfault.com/questions/870237/nginx-docker-container-immediately-stops
- https://stackoverflow.com/questions/44884719/exited-with-code-0-docker
- https://stackoverflow.com/questions/60238724/jwilder-nginx-giving-403-permission-forbidden
- https://stackoverflow.com/questions/19285355/nginx-403-error-directory-index-of-folder-is-forbidden
- https://stackoverflow.com/questions/35689628/starting-a-shell-in-the-docker-alpine-container
- https://stackoverflow.com/questions/65627946/how-to-start-nginx-server-within-alpinelatest-image-using-rc-service-command
- https://www.youtube.com/watch?v=hxngRDmHTM0&ab_channel=WesDoyle
